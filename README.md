# Hylien road to CTO

## Sommaire

- [Introduction](#introduction)
- [Schéma](#schema-de-formation)
- [Formation](#formation)

    1. [HTML](#html)
    2. [CSS](#css)
    3. [HTML & CSS](#html-css)
    4. [GIT](#git)
    5. [JavaScript](#javascript)
    6. [PHP](#php)
    7. [SQL](#sql)
    8. [Projet n°1](#projet-n1)
    9. [Composer](#composer)
    10. [Approfondissement PHP](#approfondissement-php)
    11. [Projet n°2](#projet-n2)
    12. [Symfony](#symfony)
    13. [Projet n°3](#projet-n3)

## Introduction

L'idée ici c'est de te faire voir les bases, on va y aller progressivement. Je n’aurai pas le temps d'être 100% disponible pour te former donc on va s'appuyer sur des bonnes ressources de Grafikart (que Brian a cité) et p-e d'autre si on voit que ça ne te suffit pas pour bien approfondir le sujet.

Victor l'a dit et je le redis le meilleur moyen c'est de pratiquer. Alors dans l'ordre tu vas faire :
1. la base du web le HTML (langage de balisage)
2. du CSS (langage de style)
3. du JavaScript (pour bien saisir ce qu'est un langage de programmation et avoir un exo facile par l'exemple avec le HTML)
4. Git (un outil de versionning qui te servira aussi à améliorer ta productivité, utiliser dans le travail d'équipe, etc. C'est ce que j'utilise pour ton tuto)
5. PHP (un langage idéal pour le web et surtout pour apprendre car simple mais permet d'aller loin)
6. SQL (langage te permettant de communiquer avec une base de données)
7. Composer (gestionnaire de paquet)
8. Approfondissement PHP

**/!\\ Très important /!\\**

Quand tu seras sur les tutos si jamais tu galères, tu ne comprends pas un truc ou tu as des questions n'hésite surtout pas, je suis là pour ça (et je suppose que ça sera pareil pour les autres), c'est toujours un plaisir d'aider et d'expliquer.

**N'hésite jamais à demander**

**/!\\ important /!\\**

Adapte ton temps de formation à ta disponibilité (de temps comme d'esprit) ne cherche pas à trop faire ce qui pourrait te démotiver, faire des pauses aide à mémoriser et à vider son esprit pour reprendre de plus belle. J'ai souvent debug des scripts alors que j'étais dans la rue en train de marcher ou dans mon lit en train de m'endormir.

## Schema de formation

Je vais te donner des liens Grafikart comme formation principale puis des liens Openclassroom pour des tp et des quizs mais si tu le souhaites tu peux aussi lire les cours Openclassroom qui seront liés aux liens que je vais te fournir, j'accorde juste plus de crédit à Grafikart qu'à Openclassroom.

-----

## Formation

Suis bien l'ordre dans lequel je te donne les liens. **N'hésite jamais à demander** et amuse toi bien. [:

À chaque exercice/TP il faudra que tu m'envoies/nous envoies ton code (en zip dans un premier temps, puis on feras ça sur Git dans un second temps quand tu connaîtras).

Il y a des exercices et TP Grafikart ET OC que je mets explicitement, pense à envoyer les exercices et TP des deux sites.

### HTML

1. https://grafikart.fr/formations/html
2. https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3/4004681-pratiquez
3. https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3/6725196-entrainez-vous-en-structurant-votre-cv

### CSS

1. https://grafikart.fr/formations/css
2. https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3/4214566-pratiquez
3. https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3/6725256-entrainez-vous-en-mettant-en-forme-votre-cv
4. https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3/exercises/92

### HTML & CSS

1. https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3/4214576-pratiquez
2. https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3/1606688-tp-creez-un-site-pas-a-pas
3. https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3/6725301-entrainez-vous-en-organisant-votre-cv
4. https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3/exercises/1236
5. https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3/6725346-entrainez-vous-en-adaptant-votre-cv-en-responsive
6. https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3/exercises/96

### Git

1. https://grafikart.fr/formations/git
2. https://openclassrooms.com/fr/courses/5641721-utilisez-git-et-github-pour-vos-projets-de-developpement/exercises/3448
3. https://openclassrooms.com/fr/courses/5641721-utilisez-git-et-github-pour-vos-projets-de-developpement/exercises/3442
4. https://openclassrooms.com/fr/courses/5641721-utilisez-git-et-github-pour-vos-projets-de-developpement/exercises/3444
5. https://openclassrooms.com/fr/courses/5641721-utilisez-git-et-github-pour-vos-projets-de-developpement/exercises/3449

**Instant BONUS** Très bon site pour comprendre Git : https://learngitbranching.js.org/?locale=fr_FR garde ça de côté mon cher Hylien

**/!\\ IMPORTANT /!\\** à partir d'ici on ne veut plus de zip, tout passeras par Git. [:

### JavaScript

1. https://grafikart.fr/formations/debuter-javascript
2. https://openclassrooms.com/fr/courses/6175841-apprenez-a-programmer-avec-javascript/exercises/3497
3. https://openclassrooms.com/fr/courses/6175841-apprenez-a-programmer-avec-javascript/exercises/3498
4. https://openclassrooms.com/fr/courses/6175841-apprenez-a-programmer-avec-javascript/exercises/3801
5. https://openclassrooms.com/fr/courses/5543061-ecrivez-du-javascript-pour-le-web/exercises/3133
6. https://openclassrooms.com/fr/courses/5543061-ecrivez-du-javascript-pour-le-web/exercises/3182
7. https://openclassrooms.com/fr/courses/5543061-ecrivez-du-javascript-pour-le-web/exercises/3197

### PHP

1. https://grafikart.fr/formations/php
2. https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql/exercises/1663
3. https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql/exercises/95
4. https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql/912799-transmettez-des-donnees-avec-lurl
5. https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql/913099-transmettez-des-donnees-avec-les-formulaires
6. https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql/913196-tp-page-protegee-par-mot-de-passe
7. https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql/exercises/98
8. EXO PHP CUSTOM à me demander

### SQL
Avec MySQL et PHP

1. https://grafikart.fr/formations/mysql
2. https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql/914663-tp-un-minichat
3. https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql/6964512-tp-un-blog-avec-des-commentaires
4. https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql/exercises/93
5. https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql/917948-tp-creez-un-espace-membres
6. https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql/exercises/1497

#### Projet n°1

Il va falloir réaliser un projet relativement conséquent qui utilise tous les éléments que tu as appris jusqu'ici. Tu as le champ libre et si tu n'as pas d'idée n'hésite pas à demander des idées de projet.

### Composer

1. https://grafikart.fr/tutoriels/composer-480

### Approfondissement PHP

1. https://grafikart.fr/formations/programmation-objet-php
2. https://grafikart.fr/formations/mise-pratique-poo

#### Projet n°2

Il va falloir réaliser un projet relativement conséquent qui utilise tous les éléments que tu as appris jusqu'ici (tu dois utiliser du PHP avancé comme tu viens d'apprendre). Tu as le champ libre et si tu n'as pas d'idée n'hésite pas à demander des idées de projet.
Pour PHP tu peux utiliser le framework que tu as crée ou des frameworks existants.

Très bon framework d'apprentissage : https://github.com/framework-w/framework-w

### Symfony
Le framework qui rapporte du boulot en France!

1. https://grafikart.fr/formations/symfony-4-pratique

#### Projet n°3

https://grafikart.fr/tutoriels/rest-503

Il va falloir réaliser d'api REST en Symfony tu pourras utilsier Postman pour la tester et l'utiliser.
